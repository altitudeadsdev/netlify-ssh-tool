# Netlify SSH Tool
This is a simple tool that allows us to use the normal `npm install` process without the need to add user and password 
to the package.json. The idea of the script is to be added to the package.json as a hook for the prescript.

Add the following line to your package.json:

```json
"scripts": {
  "preinstall": "bash script/netlify-ssh-setup.sh"
}
```

Then add this repo as a git submodule by running the following:

```bash
git submodule add https://bitbucket.org/altitudeadsdev/netlify-ssh-tool/ script
```

Thats it, simples.

# Resources:

https://www.atlassian.com/git/tutorials/git-submodule

https://answers.netlify.com/t/support-guide-using-an-ssh-key-via-environment-variable-during-build/2457
